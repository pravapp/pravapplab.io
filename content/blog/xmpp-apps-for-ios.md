---
title: "iOS app recommendations for XMPP"
date: 2022-03-11T00:54:07+05:30
draft: false
---
Updated on 28-April-2022

These are the XMPP apps that we recommend for iOS users.

### Snikket

[Snikket](https://snikket.org) is a fully featured app for iOS for XMPP. You need to create an XMPP account on a service and login in Snikket app. xmpp.cloud is a service we tested and that works well for iOS.

<img src="/images/snikket.png" alt="Snikket app" style="height:400px;">

### Monal IM

[Monal IM](https://monal.im) is a fully featured iOS app for XMPP. You can register accounts within the app and start using it. 

*Note: Monal IM does not support audio/video calls as of now.*

Here are some screenshots of Monal

<img src="/images/monal_1.png" alt="Monal Welcome Screen" style="height:400px;">
<img src="/images/monal_2.png" alt="Monal Registration Screen" style="height:400px;">
<img src="/images/monal_3.png" alt="Monal chat" style="height:400px;">

### Siskin IM

Siskin IM app allows creating account but a few things need adjustments: 

1. Services suggested by default needs email confirmation but it is not obvious from the app interface.

2. HTTP upload needs to be enabled by default for asynchronous file sharing. Default is jigle file transfer, which is peer to peer but both users need to be online to transfer files.

3. Subscription needs to be enabled manually after adding a contact. This is sometimes required for omemo encryption.

4. OMEMO needs to be enabled for each chat/contact from settings.

<img src="/images/siskin.jpg" alt="Siskin app" style="height:400px;">
<br>
<img src="/images/siskin_1.jpg" alt="Siskin app" style="height:400px;">

