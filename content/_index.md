---
title: 'Homepage'
meta_title: 'Prav App'
description: "Prav Messaging App- Reclaiming choice of service providers"
intro_image: "/images/prav-beta-launch-1.jpg"
intro_image_absolute: false
intro_image_hide_on_mobile: false
---
# Prav Messaging App- Reclaiming choice of service providers

Prav App is convenient without [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in). We are currently in private beta phase and planning for a public beta release of prav app.

<style>
.button
{
background-color: #ff1493;
}
<div>
</style>
<a href="/about"><button class="button">Learn More</button></a>

<a href="/beta-release-party"><button class="button">Request Beta App</button></a>
